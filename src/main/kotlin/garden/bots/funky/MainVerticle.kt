package garden.bots.funky

import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.StaticHandler

class MainVerticle : AbstractVerticle() {
  override fun stop(stopPromise: Promise<Void>) {
    stopPromise.complete()
  }

  override fun start(startPromise: Promise<Void>) {

    val breaker = getCircuitBreaker(vertx)

    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())

    /*
    REGULATOR_PORT=8080 \
    FUNKTIONS_SERVER="127.0.0.1" \
    FUNKTIONS_STORE="127.0.0.1" \
    FUNKTIONS_STORE_PORT=8090 \
    FUNKTIONS_STORE_PREFIX="funktions" \
    FUNK_PREFIX="funk" \
     */

    val httpPort = System.getenv("REGULATOR_PORT")?.toInt() ?: 8080
    // where are running all funktions runtimes
    val funktionsServer = System.getenv("FUNKTIONS_SERVER")?.toString() ?: "127.0.0.1"
    val funktionsStore = System.getenv("FUNKTIONS_STORE")?.toString() ?: "127.0.0.1"
    val funktionsStorePort = System.getenv("FUNKTIONS_STORE_PORT")?.toInt() ?: 8090
    val funktionStorePrefix = System.getenv("FUNKTIONS_STORE_PREFIX")?.toString() ?: "funktions"
    val funktionStoreToken = System.getenv("FUNKTIONS_STORE_TOKEN")?.toString() ?: "ILOVEPANDAS"

    val regulatorPrefix = System.getenv("FUNK_PREFIX")?.toString() ?: "funk"

    val client = WebClient.create(vertx) // TODO: create a pool of client ? avoid instanciation

    router.get("/${regulatorPrefix}/:functionName").handler { context ->
      val calledFunction = context.request().getParam("functionName")

      checkIfFunktionIsRunning(breaker, client, funktionsStore, "/${funktionStorePrefix}/${calledFunction}", funktionsStorePort, funktionsServer, funktionStoreToken) {
        when {
          it.isFailure -> {
            println("😡 [GET] ${it.exceptionOrNull()?.message}") // probably the funktion does not exist
            context.sendText(it.exceptionOrNull()?.message)
          }
          it.isSuccess -> {
            funktionGetCall(client, funktionsServer, "/", it.getOrNull()?.getInteger("port")!!) {
              when {
                it.isFailure -> context.sendText(it.exceptionOrNull()?.message)
                it.isSuccess -> context.reSendResponse(it.getOrNull())
              }
            }
          }
        }
      }
    }

    router.post("/${regulatorPrefix}/:functionName").handler { context ->
      val calledFunction = context.request().getParam("functionName")

      checkIfFunktionIsRunning(breaker, client, funktionsStore, "/${funktionStorePrefix}/${calledFunction}", funktionsStorePort, funktionsServer, funktionStoreToken) {
        when {
          it.isFailure -> {
            println("😡 [POST] ${it.exceptionOrNull()?.message}")
            // TODO: return JSON
            context.sendText(it.exceptionOrNull()?.message)
          }
          it.isSuccess -> {
            funktionPostCall(client, funktionsServer, "/", it.getOrNull()?.getInteger("port")!!, context.bodyAsJson) {
              when {
                it.isFailure -> context.sendText(it.exceptionOrNull()?.message)
                it.isSuccess -> context.reSendResponse(it.getOrNull())
              }
            }
          }
        }
      }
    }

    router.route("/").handler(StaticHandler.create().setCachingEnabled(false))

    vertx
      .createHttpServer()
      .requestHandler(router)
      .listen(httpPort) { http ->
        when {
          http.failed() -> {
            startPromise.fail(http.cause())
          }
          http.succeeded() -> {
            println("🤖 Regulator started on port $httpPort")
            startPromise.complete()
          }
        }
      }
  }
}
