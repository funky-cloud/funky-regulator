package garden.bots.funky

import io.vertx.circuitbreaker.CircuitBreaker
import io.vertx.kotlin.circuitbreaker.circuitBreakerOptionsOf

fun getCircuitBreaker(vertx:  io.vertx.core.Vertx): CircuitBreaker {
  val options = circuitBreakerOptionsOf(
    maxFailures = 5,
    maxRetries = 5,
    timeout = 5000,
    fallbackOnFailure = true
  )
  return CircuitBreaker.create("circuit-breaker", vertx, options).openHandler {
    println("Circuit opened")
  }.closeHandler {
    println("Circuit closed")
  }

}
