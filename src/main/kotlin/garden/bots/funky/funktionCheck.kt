package garden.bots.funky

import io.vertx.circuitbreaker.CircuitBreaker
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient

fun startFunktion(breaker: CircuitBreaker, client: WebClient, funktionsStore: String, uri: String, funktionsStorePort: Int, funktionsServer: String, funktionPort: Int, funktionInfo: JsonObject, callBack: (Result<JsonObject>) -> Unit) {
  client.get(funktionsStorePort, funktionsStore, "${uri}/start").send {
    when {

      it.failed() -> { // 😡
        callBack(Result.failure(Exception(it.cause().message)))
      }
      it.succeeded() -> { // 😀
        println("🤖😀 Funktion is starting 🎉 , you can call it soon")
        // Give time to the funktion to be started
        breaker.execute<String> { future ->
          client.get(funktionPort, funktionsServer, "/health").send {

            println("-----------------------------------------------------------------------")
            println("🔵 [health] port:${funktionPort} server: ${funktionsServer}")
            println("-----------------------------------------------------------------------")
            // test: {"checks":[{"id":"compilation","status":"DOWN"}],"outcome":"DOWN"}

            when { // 😡
              it.failed() -> {
                println("🤬 " + it.cause().message)
                // TODO: if I activate the furure.fail, the breaker is blocked ... 🤔
                //future.fail("HTTP error")
              }
              it.succeeded() -> { // 😀
                if (it.result().statusCode() != 200) {
                  println("😡 ${it.result().statusCode()}")
                  println("😡 ${it.result().statusMessage()}")
                  println("🔴 ${it.result().bodyAsString()}")
                  // 🔴 {"checks":[{"id":"compilation","status":"DOWN"}],"outcome":"DOWN"}
                  // QUESTION: is it possible to add more information to the healthcheck payload?
                  callBack(Result.failure(Exception(it.result().statusMessage())))
                  //future.fail("HTTP error")
                } else {
                  println("🟢 ${it.result().bodyAsString()}")
                  // 🟢 {"checks":[{"id":"compilation","status":"UP"}],"outcome":"UP"}
                  callBack(Result.success(funktionInfo))
                }
                future.complete()
              } // ent of succeeded
            } // end of when
          } // end of send
        } // end of breaker.execute
      } // ent of succeeded
    } // end of when
  } // end of send
}

fun checkIfFunktionIsRunning(breaker: CircuitBreaker, client: WebClient, funktionsStore: String, uri: String, funktionsStorePort: Int, funktionsServer: String, funktionStoreToken: String, callBack: (Result<JsonObject>) -> Unit) {
  // uri = funktions/function_name

  client.get(funktionsStorePort, funktionsStore, uri).putHeader("funky-store-token",funktionStoreToken).send { it ->
    when {
      // 😡
      it.failed() -> {
        println("🖐 ${it.cause().message}")
        callBack(Result.failure(Exception(it.cause().message)))
      }
      // 😀
      it.succeeded() -> {

        val funktionInfo = it.result().bodyAsJsonObject()
        //println("🖐 $funktionInfo")

        if(funktionInfo?.getBoolean("running") == null) {
          callBack(Result.failure(Exception("🖐😡 Funktion does not exist")))
        } else { // funktion exists
          val funktionIsRunning = funktionInfo.getBoolean("running")
          val funktionPort = funktionInfo.getInteger("port")

          if(funktionIsRunning) {
            println("🤖😀 Funktion is running, you can call it")
            callBack(Result.success(funktionInfo))
          } else {
            println("🤖😡 Funktion is not running, we need to start it")
            startFunktion(breaker,client,funktionsStore,uri,funktionsStorePort,funktionsServer,funktionPort, funktionInfo) {
              when {
                it.isFailure -> callBack(Result.failure(Exception(it.exceptionOrNull()?.message)))
                it.isSuccess -> callBack(Result.success(funktionInfo))
              }
            }
          } // end if

        }


      } // end succeed
    } // end when
  } // end send
}
