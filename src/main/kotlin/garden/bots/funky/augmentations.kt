package garden.bots.funky

import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.HttpResponse

fun RoutingContext.sendText(text: String?) {
  this.response().putHeader("content-type", "text/html;charset=UTF-8").end(text)
}

fun RoutingContext.reSendResponse(resp: HttpResponse<Buffer>?) {

  this.response().putHeader("content-type", resp?.getHeader("content-type"))
    .setStatusCode(resp?.statusCode()!!).setStatusMessage(resp.statusMessage())
    .end(resp.bodyAsString())
}
