package garden.bots.funky

import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient

fun funktionGetCall(client: WebClient, server: String, uri: String, httpPort: Int, callBack: (Result<HttpResponse<Buffer>>) -> Unit) {

  client.get(httpPort, server, "/").send { ar ->
    when {
      ar.failed() -> { // 😡
        callBack(Result.failure(Exception("666:${ar.cause().message}")))
      }
      ar.succeeded() -> { // 😀
        val funktionResponse = ar.result()
        val text = "${funktionResponse.statusCode()}:${funktionResponse.statusMessage()}"
        println("😀 Received response ${text}")
        //https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
        funktionResponse.statusCode().let {
          when(it) {
            in 400..599 -> {
              callBack(Result.failure(Exception(text)))
            }
            else -> { // TODO test the status code 200, ...
              callBack(Result.success(funktionResponse))
            }
          } // end of when(it)
        } // end of response.statusCode()
      } // end of succeed
    } // end of first when
  } // end of client
}
